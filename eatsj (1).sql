-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 31 jan. 2022 à 16:28
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `eatsj`
--

-- --------------------------------------------------------

--
-- Structure de la table `boisson`
--

DROP TABLE IF EXISTS `boisson`;
CREATE TABLE IF NOT EXISTS `boisson` (
  `id_boisson` bigint(20) NOT NULL,
  `boisson_pic` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `prix` double NOT NULL,
  `restaurateur_id_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_boisson`),
  KEY `FKiciyrsohfmf9yu5y9htvbhnlo` (`restaurateur_id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `boisson`
--

INSERT INTO `boisson` (`id_boisson`, `boisson_pic`, `nom`, `prix`, `restaurateur_id_user`) VALUES
(13, NULL, 'Coca-Cola', 500, 4),
(14, NULL, 'Baobab', 700, 4);

-- --------------------------------------------------------

--
-- Structure de la table `boisson_commande`
--

DROP TABLE IF EXISTS `boisson_commande`;
CREATE TABLE IF NOT EXISTS `boisson_commande` (
  `commande_id` bigint(20) NOT NULL,
  `boisson_id` bigint(20) NOT NULL,
  KEY `FKe42ktx8nusl6uxseedwdrjvmg` (`boisson_id`),
  KEY `FKrif7gjshq5lnn5r1kc1eiqrgt` (`commande_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `id_commande` bigint(20) NOT NULL,
  `date_commande` datetime DEFAULT NULL,
  `etat` varchar(255) DEFAULT NULL,
  `formule` varchar(255) DEFAULT NULL,
  `prix_totale` float NOT NULL,
  `qte_boissons` int(11) NOT NULL,
  `qte_plats` int(11) NOT NULL,
  `consommateur_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `FKo4b5qaco085q2ecn9123wiir` (`consommateur_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `commande`
--

INSERT INTO `commande` (`id_commande`, `date_commande`, `etat`, `formule`, `prix_totale`, `qte_boissons`, `qte_plats`, `consommateur_id`) VALUES
(16, '2022-01-21 14:40:22', 'PAS BON', 'Petite Faim', 1500, 0, 3, 3),
(17, '2021-12-30 14:42:09', 'PAS BON', 'Standard', 1400, 0, 2, 3),
(18, '2022-01-30 14:47:58', 'PAS BON', 'Standard', 4200, 0, 6, 3),
(25, '2022-01-19 09:12:36', 'PAS BON', 'Grande Faim', 3000, 0, 3, 10),
(33, '2022-01-31 13:38:23', 'PAS BON', 'Grande Faim', 4000, 0, 4, 32);

-- --------------------------------------------------------

--
-- Structure de la table `commentaire`
--

DROP TABLE IF EXISTS `commentaire`;
CREATE TABLE IF NOT EXISTS `commentaire` (
  `id_commentaire` bigint(20) NOT NULL,
  `date_commentaire` datetime DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_commentaire`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `commentaire_plat`
--

DROP TABLE IF EXISTS `commentaire_plat`;
CREATE TABLE IF NOT EXISTS `commentaire_plat` (
  `commentaire_id` bigint(20) NOT NULL,
  `plat_id` bigint(20) NOT NULL,
  KEY `FKhatx3imb1rmn9jt9jh58x0vk7` (`plat_id`),
  KEY `FK1vvb1jveu4e1jfkmshfoyi7fu` (`commentaire_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(34);

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id_note` bigint(20) NOT NULL,
  `date_note` datetime DEFAULT NULL,
  `nombre_note` float NOT NULL,
  PRIMARY KEY (`id_note`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `note_plat`
--

DROP TABLE IF EXISTS `note_plat`;
CREATE TABLE IF NOT EXISTS `note_plat` (
  `note_id` bigint(20) NOT NULL,
  `plat_id` bigint(20) NOT NULL,
  KEY `FKfy6778fs5vgvyu4uutnwu3y7o` (`plat_id`),
  KEY `FK2kqa9x67eiopvqs9dvxw69hoy` (`note_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `plat`
--

DROP TABLE IF EXISTS `plat`;
CREATE TABLE IF NOT EXISTS `plat` (
  `id_plat` bigint(20) NOT NULL,
  `complemment` varchar(255) DEFAULT NULL,
  `nom_plat` varchar(255) DEFAULT NULL,
  `plat_pic` varchar(255) DEFAULT NULL,
  `sauce` varchar(255) DEFAULT NULL,
  `restaurateur_id_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_plat`),
  KEY `FK9ijxiu83fg59ygys1toc65qcs` (`restaurateur_id_user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `plat`
--

INSERT INTO `plat` (`id_plat`, `complemment`, `nom_plat`, `plat_pic`, `sauce`, `restaurateur_id_user`) VALUES
(5, 'BOBOLO', 'OKOK SUCRE', NULL, 'OKOK', 4),
(7, 'Waterfufu', 'ERU AVEC BEAUCOUP D\'OBSTACLE', NULL, 'ERU', 6),
(11, 'taro', 'bon taro sauce jaune', NULL, 'sauce jaune', 4),
(12, 'PLANTAIN', 'KOKI PLANTAIN ', NULL, 'KOKI', 4);

-- --------------------------------------------------------

--
-- Structure de la table `plat_commande`
--

DROP TABLE IF EXISTS `plat_commande`;
CREATE TABLE IF NOT EXISTS `plat_commande` (
  `commande_id` bigint(20) NOT NULL,
  `plat_id` bigint(20) NOT NULL,
  KEY `FK5ra4srtmb0nh3phnf42wjig4` (`plat_id`),
  KEY `FK1n7nnuf2pou9cxskpfaq17yg2` (`commande_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `plat_commande`
--

INSERT INTO `plat_commande` (`commande_id`, `plat_id`) VALUES
(16, 5),
(17, 5),
(18, 11),
(25, 7),
(33, 12);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id_role` bigint(20) NOT NULL,
  `nom_role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_role`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id_role`, `nom_role`) VALUES
(1, 'Restaurateur'),
(2, 'Consommateur');

-- --------------------------------------------------------

--
-- Structure de la table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id_role` bigint(20) NOT NULL,
  `user_id_user` bigint(20) NOT NULL,
  UNIQUE KEY `UK_iu5veq9cveafy5n6cp94uiufl` (`user_id_user`),
  KEY `FKfaxiblgsogq1mvxaqfu5ff7fc` (`role_id_role`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id_user` bigint(20) NOT NULL,
  `login` varchar(255) DEFAULT NULL,
  `nom_user` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_pic` varchar(255) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  UNIQUE KEY `UK_ew1hvam8uwaknuaellwhqchhb` (`login`),
  KEY `FKn82ha3ccdebhokx3a8fgdqeyy` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id_user`, `login`, `nom_user`, `password`, `user_pic`, `role_id`) VALUES
(3, 'fabrice', 'Fabrice TOUOMOU', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 2),
(4, 'hilary', 'Hilary Nzamboue', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 1),
(6, 'dozo', 'Cedric Armel', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 1),
(10, 'fabricetouomou84@gmail.com', 'Fabrice_T', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 2),
(15, 'aicha', 'AICHA', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 1),
(19, 'Mary', 'Mary', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 1),
(32, 'cheikh', 'Cheikh Kacfah', '4a7d1ed414474e4033ac29ccb8653d9b', NULL, 2);


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
