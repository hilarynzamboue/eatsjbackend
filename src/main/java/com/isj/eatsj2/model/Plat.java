package com.isj.eatsj2.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;


@Entity
@Data
@NoArgsConstructor
@ToString
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "idPlat")
public class Plat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idPlat;

    private String nomPlat;

    private String sauce;

    private String complemment;

    public Plat( String sauce, String complemment){
        this.sauce = sauce;
        this.complemment = complemment;
        this.nomPlat = this.sauce + " avec " + this.complemment;
    }

    @ManyToMany(mappedBy = "platsCommande")
    @JsonIgnore
    private List<Commande> commandes;

    @ManyToMany(mappedBy = "platsCommentaire")
    @JsonIgnore
    private List<Commentaire> commentaires;

    @ManyToMany(mappedBy = "platsNote")
    @JsonIgnore
    private List<Note> notes;

    @ManyToOne()

    //@JsonIgnore

//    @JsonIgnore

    @JoinColumn(name = "restaurateur_id")
    private User restaurateur;

    @Column(length = 100000)
    private byte[] platPic;
    
}
