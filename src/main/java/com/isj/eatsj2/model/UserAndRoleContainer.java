package com.isj.eatsj2.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

//@NoArgsConstructor
@Getter
@Setter
public class UserAndRoleContainer implements Serializable {

    private User user;
    private Role role;

    public UserAndRoleContainer(User user, Role role) {
        this.user = user;
        this.role = role;
    }
    public UserAndRoleContainer()
    {this.user=null;this.role=null;}

}
