package com.isj.eatsj2.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

// import java.sql.Date;

import javax.persistence.*;
// import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Data
@Getter
@Setter
@NoArgsConstructor
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "idCommande")
@ToString
public class Commande implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idCommande;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateCommande;

    private String etat;

    private String type;

    private String formule;

    private float prixTotale;

    private int qtePlats;

    private int qteBoissons;

    public  Commande (String etat, String formule, float prixTotale, Date date, String type){
        this.dateCommande=date;
        this.etat = etat;
        this.type = type;
        this.formule = formule;
        this.prixTotale = prixTotale;
        this.qteBoissons = 0;
        this.qtePlats = 0;

    }

//    public Commande(String etat, String formule, float prixTotale){}


    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference
//    @JsonIgnore
    @JoinColumn(name = "consommateur_id" )
    private User consommateur;


    
    @ManyToMany()
    @JsonIgnore
    @JoinTable(name = "boisson_commande", 
      joinColumns = @JoinColumn(name = "commande_id", referencedColumnName = "idCommande"), 
      inverseJoinColumns = @JoinColumn(name = "boisson_id", 
      referencedColumnName = "idBoisson"))
    private List<Boisson> boissons;

    @ManyToMany()
    @JsonIgnore
    @JoinTable(name = "plat_commande", 
      joinColumns = @JoinColumn(name = "commande_id", referencedColumnName = "idCommande"),
      inverseJoinColumns = @JoinColumn(name = "plat_id", 
      referencedColumnName = "idPlat"))
    private List<Plat> platsCommande;


    @JsonBackReference
//    @JsonIgnore
    public User getConsommateur() {
        return this.consommateur;
    }

    public void setIdCommande(long idCommande) {
        this.idCommande = idCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setFormule(String formule) {
        this.formule = formule;
    }

    public void setPrixTotale(float prixTotale) {
        this.prixTotale = prixTotale;
    }

    public void setQtePlats(int qtePlats) {
        this.qtePlats = qtePlats;
    }

    public void setQteBoissons(int qteBoissons) {
        this.qteBoissons = qteBoissons;
    }

    public void setConsommateur(User consommateur) {
        this.consommateur = consommateur;
    }

    public void setBoissons(List<Boisson> boissons) {
        this.boissons = boissons;
    }

    public void setPlatsCommande(List<Plat> platsCommande) {
        this.platsCommande = platsCommande;
    }
}
