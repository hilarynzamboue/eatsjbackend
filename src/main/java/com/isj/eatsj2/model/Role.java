package com.isj.eatsj2.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.JsonGenerator;
import com.isj.eatsj2.config.EntityIdResolver;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import lombok.*;

import javax.persistence.*;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
//@Transactional au service
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "idRole",resolver = EntityIdResolver.class,scope = Role.class)
public class Role implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idRole;

    private String nomRole;

    public Role(String nomRole){
        this.nomRole=nomRole;
    }

    @OneToMany(mappedBy = "role")
    @JsonManagedReference
    @JsonIgnore
    private List<User> users;

    @JsonManagedReference
    @JsonIgnore
    public List<User> getUsers(){return this.users;}
}
