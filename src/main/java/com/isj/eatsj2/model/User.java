package com.isj.eatsj2.model;


import com.fasterxml.jackson.annotation.*;
import com.isj.eatsj2.config.EntityIdResolver;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

// import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class,property = "idUser",resolver = EntityIdResolver.class,scope = User.class)
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idUser;

    private String nomUser;

    @Column(unique = true )
    private String login;

    private String password;

    @Column(length = 100000)
    private byte[] userPic;

    public User(String nomUser, String login, String password){
        this.nomUser=nomUser;
        this.login = login;
        this.password=password;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserPic(byte[] userPic) {
        this.userPic = userPic;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @OneToMany(mappedBy = "consommateur")
    @JsonManagedReference
    @JsonIgnore
    private List<Commande> commandes;

    @JsonManagedReference
    @JsonIgnore
    public List<Commande> getCommandes() {
        return this.commandes;
    }

    @ManyToOne
    @JoinColumn(name = "role_id")
    @JsonBackReference
    @JsonIgnore
    private Role role;

    @JsonBackReference
    @JsonIgnore
    public Role getRole(){return this.role;}

    public void setRole(Role role) {
        this.role = role;
    }
}
