package com.isj.eatsj2.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;

import java.io.Serializable;
import java.util.List;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;


@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Note implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idNote;

    private float nombreNote;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dateNote;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinTable(name = "note_plat", 
      joinColumns = @JoinColumn(name = "note_id", referencedColumnName = "idNote"), 
      inverseJoinColumns = @JoinColumn(name = "plat_id", 
      referencedColumnName = "idPlat"))
    private List<Plat> platsNote;
}
