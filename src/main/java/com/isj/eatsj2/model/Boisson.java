package com.isj.eatsj2.model;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.AccessLevel;

import javax.persistence.*;
// import javax.persistence.JoinColumn;
// import javax.persistence.JoinTable;

//@Builder
@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@ToString
public class Boisson implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idBoisson;
    private String nom;
    private double prix;

    public Boisson(String name, double price){
        this.nom= name;
        this.prix=price;
    }

    @ManyToMany(mappedBy = "boissons")
    @JsonIgnore
    private List<Commande> commandes;

    @ManyToOne()
    //@JsonIgnore
    @JoinColumn(name = "restaurateur_id")
    private User restaurateur;

    @Column(length = 100000)
    private byte[] boissonPic;

}
