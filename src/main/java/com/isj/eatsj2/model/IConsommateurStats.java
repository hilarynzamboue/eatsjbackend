package com.isj.eatsj2.model;

public interface IConsommateurStats {
    Long getDepensesTotales();
    Long getNombreDePlats();
    Double getPoints();
    String getConsommateur();
    Long getIdUser();
}
