package com.isj.eatsj2;

import com.isj.eatsj2.model.Commande;
import com.isj.eatsj2.model.Plat;
import com.isj.eatsj2.repository.PlatRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
public class Eatsj2Application {

	public static void main(String[] args) {
		SpringApplication.run(Eatsj2Application.class, args);
	}
//	@Bean
//	CommandLineRunner init(PlatRepository platRepository) {
//		return args -> {
//			Stream.of("OKOK", "POULET", "POISSON", "PORC", "NDOLE").forEach(name -> {
//				Plat plat = new Plat(name, "BOBOLO");
//				platRepository.save(plat);
//			});
//			platRepository.findAll().forEach(System.out::println);
//		};
//	}

}
