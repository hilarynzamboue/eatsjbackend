package com.isj.eatsj2.services;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class UsefulService {
    public static LocalDate convertDateToLocalDate(Date dateToConvert){
        return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }

    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException ioe) {
        } catch (DataFormatException e) {
        }
        return outputStream.toByteArray();
    }


    //    @GetMapping(path = { "/get/{imageName}" })
//    public Image getImage(@PathVariable("imageName") String imageName) throws IOException {
//        final Optional<Image> retrievedImage = imageRepository.findByName(imageName);
//        Image img = new Image(retrievedImage.get().getName(), retrievedImage.get().getType(),
//                decompressBytes(retrievedImage.get().getPicByte()));
//        return img;
//    }


    //    @PostMapping("/upload")
//    public BodyBuilder uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
//        Image img = new Image(file.getOriginalFilename(), file.getContentType(),
//                compressBytes(file.getBytes()));
////        imageRepository.save(img);
//        return ResponseEntity.status(HttpStatus.OK);
//    }

    //    @GetMapping(path = { "/get" })
//    public User getImage() throws IOException {
//        User user = iUserService.rechercherUser(1L);
//        user.setUserPic(decompressBytes(user.getUserPic()));
//        return user;
//    }
}
