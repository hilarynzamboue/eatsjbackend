package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Plat;
import com.isj.eatsj2.model.User;

public interface iPlatService {
    List<Plat> listePlats();
    List<Plat> listePlatsFav(Long idRestaurateur);
    Plat rechercherPlatParId(Long id);
    Plat enregistrerPlat(Plat plat);
    List<Plat> supprimerPlat(Long id);
    List<Plat> platsFavorisSurNJours(Long idRestaurateur,int nombreDeJours);
    List<Plat> platsParOrdreVentes(Long idRestaurateur);
    List<Plat> platsParOrdreVentesSurNJours(Long idRestaurateur, int nombreDeJours);
    List<Plat> mesPlats(Long idRestaurateur);
    List<Plat> rechercherPlatParNomPlat(String mot_cle);
    Long SommeTotaleGagne(Long idRestaurateur);
    Long SommeTotaleGagneSurNJours(Long idRestaurateur, int nombreDeJours);
}
