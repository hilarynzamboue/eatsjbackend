package com.isj.eatsj2.services;

import com.isj.eatsj2.model.IConsommateurStats;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface iStatsService {
    List<IConsommateurStats> leaderBoards(int nombreDeJours);
    List<IConsommateurStats> leaderBoardsUnRestaurateur(Long idRestaurateur,int nombreDeJours);
}
