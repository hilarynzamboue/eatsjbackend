package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Boisson;
import com.isj.eatsj2.repository.BoissonRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BoissonServiceImp implements iBoissonService{

    @Autowired
    private BoissonRepository bRepository;

    @Override
    public List<Boisson> listeBoissons(){
        return bRepository.findAll();
    }

    @Override
    public List<Boisson> mesBoissons(Long idRestaurateur) {
        return bRepository.findAllByRestaurateur(idRestaurateur);
    }

    @Override
    public Boisson rechercherBoisson(Long id){
        return bRepository.findById(id).get();
    };

    @Override
    public Boisson enregistrerBoisson(Boisson boisson){
        return bRepository.save(boisson);
    };
    public void supprimerBoisson(Long id){
        bRepository.deleteById(id);
    };
    
}
