package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Commentaire;

public interface iCommentaireService {
    List<Commentaire> listeCommentaires();
    Commentaire rechercherCommentaire(Long id);
    Commentaire enregistrerCommentaire(Commentaire commentaire);
    void supprimerCommentaire(Long id);
    List<Commentaire> commentaireDunPlat(Long idPlat);
}
