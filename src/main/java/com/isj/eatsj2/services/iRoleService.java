package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Role;

import java.util.List;

public interface iRoleService {
    List<Role> listeRoles();
    Role enregistrerRole(Role role);
    Role rechercherRole(Long id);
    void supprimerRole(Long id);
}
