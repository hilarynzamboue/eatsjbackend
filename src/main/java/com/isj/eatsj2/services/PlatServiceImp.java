package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Plat;
import com.isj.eatsj2.model.User;
import com.isj.eatsj2.repository.PlatRepository;

import com.mysql.cj.xdevapi.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import javax.xml.transform.Result;
import java.util.List;

import java.sql.*;

@Service
public class PlatServiceImp implements iPlatService {
    @Autowired
    private PlatRepository platRepository;

    @Override
    public List<Plat> listePlats(){
        return platRepository.findAll();
    }

    @Override
    public Plat rechercherPlatParId(Long id){
        return platRepository.findById(id).get();
    }

    @Override
    public Plat enregistrerPlat(Plat plat){
        return platRepository.save(plat);
    }

    @Override
    public List<Plat> supprimerPlat(Long id){
        platRepository.deleteById(id);
        return platRepository.findAll();
    }

    //Nos fonctions non CRUD

    @Override
    public List<Plat> listePlatsFav(Long idRestaurateur){return platRepository.PlatsParFavoris(idRestaurateur);}

    @Override
    public List<Plat> platsFavorisSurNJours(Long idRestaurateur, int nombreDeJours){ return platRepository.PlatsParFavorisSurNJours(idRestaurateur,nombreDeJours);}

    @Override
    public List<Plat> platsParOrdreVentes(Long idRestaurateur){return platRepository.PlatsParOrdresDeGains(idRestaurateur);}

    @Override
    public List<Plat> platsParOrdreVentesSurNJours(Long idRestaurateur, int nombreDeJours) {
        return platRepository.PlatsParOrdresDeGainsSurNJours(idRestaurateur, nombreDeJours);
    }

    @Override
    public List<Plat> mesPlats(Long idRestaurateur) {
        return platRepository.findAllByRestaurateur(idRestaurateur);
    }

    @Override
    public List<Plat> rechercherPlatParNomPlat(String mot_cle) {
        return platRepository.findAllByNomPlatContains(mot_cle);
    }

    @Override
    public Long SommeTotaleGagne(Long idRestaurateur) {
        return platRepository.SommeTotaleVendue(idRestaurateur);
    }

    @Override
    public Long SommeTotaleGagneSurNJours(Long idRestaurateur, int nombreDeJours) {
        return platRepository.SommeTotaleVendueSurNJours(idRestaurateur, nombreDeJours);
    }
}
