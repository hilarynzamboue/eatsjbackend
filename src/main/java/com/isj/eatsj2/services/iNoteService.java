package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Note;

public interface iNoteService {
    List<Note> listeNotes();
    Note rechercherNote(Long id);
    Note enregistrerNote(Note note);
    void supprimerNote(Long id);
    List<Note> notesDunPlat(Long idPlat);
}
