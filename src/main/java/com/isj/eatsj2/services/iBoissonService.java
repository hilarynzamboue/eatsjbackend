package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Boisson;

public interface iBoissonService {
    List<Boisson> listeBoissons();
    List<Boisson> mesBoissons(Long idRestaurateur);
    Boisson rechercherBoisson(Long id);
    Boisson enregistrerBoisson(Boisson boisson);
    void supprimerBoisson(Long id);
}
