package com.isj.eatsj2.services;

import com.isj.eatsj2.model.User;
import com.isj.eatsj2.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImp implements iUserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public List<User> listeUsers(){
        return userRepository.findAll();
    }

    @Override
    public User rechercherUser(Long id){
        return userRepository.findById(id).get();
    }

    @Override
    public User enregistrerUser(User user){
        return userRepository.save(user);
    }

    @Override
    public List<User> supprimerUser(Long id){
        userRepository.deleteById(id);
        return userRepository.findAll();
    }

    @Override
    public User trouverUserParIdentifiant(String login, String password) {
        return userRepository.findUserByLoginAndPassword(login,password);
    }

    @Override
    public List<User> mesclients(Long idRestaurateur) {
        return userRepository.findAllCustomers(idRestaurateur);
    }
}
