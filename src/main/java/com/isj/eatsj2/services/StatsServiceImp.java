package com.isj.eatsj2.services;

import com.isj.eatsj2.model.IConsommateurStats;
import com.isj.eatsj2.repository.StatsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StatsServiceImp implements iStatsService{
    @Autowired
    StatsRepository statsRepository;
    @Override
    public List<IConsommateurStats> leaderBoards(int nombreDeJours) {
        return statsRepository.etudiantsFidelesSurNJours(nombreDeJours);
    }

    @Override
    public List<IConsommateurStats> leaderBoardsUnRestaurateur(Long idRestaurateur, int nombreDeJours) {
        return statsRepository.etudiantsFidelesSurNJoursPourUnVendeur(idRestaurateur, nombreDeJours);
    }
}
