package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Commentaire;
import com.isj.eatsj2.repository.CommentaireRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentaireServiceImp implements iCommentaireService {

    @Autowired
    private CommentaireRepository cRepository;

    @Override
    public List<Commentaire> listeCommentaires(){
        return cRepository.findAll();
    }

    @Override
    public Commentaire rechercherCommentaire(Long id){
        return cRepository.findById(id).get();
    }

    @Override
    public Commentaire enregistrerCommentaire(Commentaire commentaire){
        return cRepository.save(commentaire);
    }

    @Override
    public void supprimerCommentaire(Long id){
        cRepository.deleteById(id);
    }

    @Override
    public List<Commentaire> commentaireDunPlat(Long idPlat) {
        return cRepository.findAllByPlat(idPlat);
    }
}
