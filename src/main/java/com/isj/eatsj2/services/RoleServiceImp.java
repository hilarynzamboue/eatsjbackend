package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Role;
import com.isj.eatsj2.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImp implements iRoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<Role> listeRoles() {
        return roleRepository.findAll();
    }

    @Override
    public Role enregistrerRole(Role role) {
        return roleRepository.save(role);
    }

    @Override
    public Role rechercherRole(Long id) {
        return roleRepository.findById(id).get();
    }

    @Override
    public void supprimerRole(Long id) {
        roleRepository.deleteById(id);
    }
}
