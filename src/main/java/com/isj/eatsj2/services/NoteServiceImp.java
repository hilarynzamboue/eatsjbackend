package com.isj.eatsj2.services;

import com.isj.eatsj2.model.Note;
import com.isj.eatsj2.repository.NoteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NoteServiceImp implements iNoteService {
    @Autowired
    private NoteRepository nRepository;

    @Override
    public List<Note> listeNotes(){
        return nRepository.findAll();
    }

    @Override
    public Note rechercherNote(Long id){
        return nRepository.findById(id).get();
    }

    @Override
    public Note enregistrerNote(Note note){
        return nRepository.save(note);
    }

    @Override
    public void supprimerNote(Long id){
        nRepository.deleteById(id);
    }

    @Override
    public List<Note> notesDunPlat(Long idPlat) {
        return nRepository.findAllByPlat(idPlat);
    }
}
