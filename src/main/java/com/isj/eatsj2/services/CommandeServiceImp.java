package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Commande;
import com.isj.eatsj2.repository.CommandeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommandeServiceImp implements iCommandeService {

    @Autowired
    private CommandeRepository cRepository;

    @Override
    public List<Commande> listeCommandes(){
        return cRepository.findAll();
    }

    @Override
    public Commande rechercherCommande(Long id){
        return cRepository.findById(id).get();
    }

    @Override
    public Commande enregistrerCommande(Commande commande){
        return cRepository.save(commande);
    }

    @Override
    public void supprimerBoisson(Long id){
        cRepository.deleteById(id);
    }

    @Override
    public List<Commande> commandesNonLivre(Long idRestaurateur, String etat) {
        return cRepository.findAllByEtatEquals(idRestaurateur,etat);
    }

    @Override
    public List<Commande> toutesMesCommandes(Long idRestaurateur) {
        return cRepository.findAllByRestaurateur(idRestaurateur);
    }

}
