package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.User;

public interface iUserService {

    List<User> listeUsers();
    User rechercherUser(Long id);
    User enregistrerUser(User user);
    List<User> supprimerUser(Long id);
    User trouverUserParIdentifiant(String login, String mot_de_passe);
    List<User> mesclients(Long idRestaurateur);
    
}
