package com.isj.eatsj2.services;

import java.util.List;

import com.isj.eatsj2.model.Commande;

public interface iCommandeService {
    List<Commande> listeCommandes();
    Commande rechercherCommande(Long id);
    Commande enregistrerCommande(Commande commande);
    void supprimerBoisson(Long id);
    List<Commande> commandesNonLivre(Long idRestaurateur, String etat);
    List<Commande> toutesMesCommandes(Long idRestaurateur);
}
