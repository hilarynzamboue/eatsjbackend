package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Boisson;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BoissonRepository extends JpaRepository<Boisson,Long> {

    @Query(value = "SELECT b from Boisson b where b.restaurateur.idUser = :idRestaurateur")
    List<Boisson> findAllByRestaurateur(@Param("idRestaurateur") Long idrestaurateur);


}
