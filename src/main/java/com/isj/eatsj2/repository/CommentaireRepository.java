package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Commentaire;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentaireRepository extends JpaRepository<Commentaire,Long> {

    @Query(value = "SELECT commentaire.id_commentaire, commentaire.date_commentaire, commentaire.message " +
            "FROM plat INNER JOIN commentaire_plat ON plat.id_plat = commentaire_plat.plat_id INNER JOIN commentaire ON commentaire_plat.commentaire_id = commentaire.id_commentaire " +
            "WHERE plat.id_plat = :idPlat ", nativeQuery = true)
    List<Commentaire> findAllByPlat(@Param("idPlat") Long idPlat);
    
}
