package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Commande;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommandeRepository extends JpaRepository<Commande,Long> {

    @Query(value = "SELECT commande.id_commande, commande.date_commande, commande.etat, commande.formule, commande.prix_totale, commande.qte_boissons, commande.qte_plats, commande.consommateur_id, commande.type " +
            "FROM commande INNER JOIN plat_commande ON commande.id_commande = plat_commande.commande_id INNER JOIN plat ON plat_commande.plat_id = plat.id_plat " +
            "WHERE plat.restaurateur_id = :idRestaurateur AND commande.etat LIKE :etat ", nativeQuery = true)
    List<Commande> findAllByEtatEquals(@Param("idRestaurateur") Long idRestaurateur,@Param("etat") String etat);


    @Query(value = "SELECT commande.id_commande, commande.date_commande, commande.etat, commande.formule, commande.prix_totale, commande.qte_boissons, commande.qte_plats, commande.type, commande.consommateur_id " +
            "FROM commande INNER JOIN plat_commande ON commande.id_commande = plat_commande.commande_id INNER JOIN plat ON plat_commande.plat_id = plat.id_plat " +
            "WHERE plat.restaurateur_id = :idRestaurateur ", nativeQuery = true)
    List<Commande> findAllByRestaurateur(@Param("idRestaurateur") Long idRestaurateur);
}
