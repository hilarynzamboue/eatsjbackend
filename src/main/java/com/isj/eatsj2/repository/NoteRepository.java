package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Note;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note,Long> {

    @Query(value = "SELECT note.id_note, note.date_note, note.nombre_note" +
            " FROM plat INNER JOIN note_plat ON plat.id_plat = note_plat.plat_id INNER JOIN note ON note_plat.note_id = note.id_note " +
            "WHERE plat.id_plat = :idPlat ", nativeQuery = true)
    List<Note> findAllByPlat(@Param("idPlat") Long idPlat);
    
}
