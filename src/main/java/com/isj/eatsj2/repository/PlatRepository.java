package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Plat;

import com.isj.eatsj2.model.User;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlatRepository extends JpaRepository<Plat,Long> {
    @Query(value = "SELECT p.id_plat,p.complemment,p.nom_plat,p.sauce,p.plat_pic,p.restaurateur_id\n" +
            "FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idrestaurateur) p\n" +
            "INNER JOIN plat_commande\n" +
            "ON p.id_plat = plat_commande.plat_id\n" +
            "INNER JOIN commande\n" +
            "ON plat_commande.commande_id = commande.id_commande\n" +
            "GROUP BY p.id_plat \n" +
            "ORDER BY SUM(commande.qte_plats) DESC",nativeQuery = true)
    List<Plat> PlatsParFavoris(@Param("idrestaurateur") Long idrestaurateur);


    @Query(value = "SELECT p.id_plat,p.complemment,p.nom_plat,p.sauce,p.plat_pic,p.restaurateur_id" +
            " FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM plat WHERE plat.restaurateur_id=:idRestaurateur) p " +
            "INNER JOIN ( SELECT plat_commande.commande_id,plat_commande.plat_id,com.qte_plats FROM plat_commande,(SELECT * FROM `commande` WHERE DATE(date_commande) >= DATE_SUB(DATE(NOW()), INTERVAL :days DAY)) com WHERE plat_commande.commande_id = com.id_commande  ) pc" +
            " ON pc.plat_id = p.id_plat GROUP BY p.id_plat ORDER BY SUM(pc.qte_plats) DESC ;",nativeQuery = true)
    List<Plat> PlatsParFavorisSurNJours(@Param("idRestaurateur") Long idRestaurateur, @Param("days") int days);


    @Query(value = "SELECT p.id_plat,p.complemment,p.nom_plat,p.sauce,p.plat_pic,p.restaurateur_id " +
            " FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idRestaurateur) p" +
            " INNER JOIN plat_commande" +
            " ON p.id_plat = plat_commande.plat_id" +
            " INNER JOIN commande" +
            " ON plat_commande.commande_id = commande.id_commande" +
            " GROUP BY p.id_plat" +
            " ORDER BY SUM(commande.prix_totale) DESC",nativeQuery = true)
    List<Plat> PlatsParOrdresDeGains(@Param("idRestaurateur") Long idRestaurateur);


    @Query(value = "SELECT p.id_plat,p.complemment,p.nom_plat,p.sauce,p.plat_pic,p.restaurateur_id" +
            " FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idRestaurateur) p" +
            " INNER JOIN plat_commande" +
            " ON p.id_plat = plat_commande.plat_id" +
            " INNER JOIN (SELECT * FROM `commande` WHERE DATE(date_commande) >= DATE_SUB(DATE(NOW()), INTERVAL :days DAY)) com" +
            " ON plat_commande.commande_id = com.id_commande" +
            " GROUP BY p.id_plat" +
            " ORDER BY SUM(com.prix_totale) DESC",nativeQuery = true)
    List<Plat> PlatsParOrdresDeGainsSurNJours(@Param("idRestaurateur") Long idrestaurateur, @Param("days") int days);


    @Query(value = "SELECT SUM(commande.prix_totale) AS Entrees" +
            " FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idrestaurateur) p" +
            " INNER JOIN plat_commande" +
            " ON p.id_plat = plat_commande.plat_id" +
            " INNER JOIN commande" +
            " ON plat_commande.commande_id = commande.id_commande",nativeQuery = true)
    Long SommeTotaleVendue(@Param("idrestaurateur") Long idrestaurateur);


    @Query(value = "SELECT SUM(com.prix_totale) AS Entrees " +
            "FROM (SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idrestaurateur) p " +
            "INNER JOIN plat_commande " +
            "ON p.id_plat = plat_commande.plat_id " +
            "INNER JOIN (SELECT * FROM `commande` WHERE DATE(date_commande) >= DATE_SUB(DATE(NOW()), INTERVAL :days DAY)) com " +
            "ON plat_commande.commande_id = com.id_commande",nativeQuery = true)
    Long SommeTotaleVendueSurNJours(@Param("idrestaurateur") Long idrestaurateur, @Param("days") int days);


    @Query(value = "SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id FROM `plat` WHERE plat.restaurateur_id=:idRestaurateur", nativeQuery = true)
    List<Plat> findAllByRestaurateur(@Param("idRestaurateur") Long idrestaurateur);

    List<Plat> findAllByNomPlatContains(String mot_cle);
}
