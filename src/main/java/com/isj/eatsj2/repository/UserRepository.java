package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.Role;
import com.isj.eatsj2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByLoginAndPassword(String login, String password);

    @Query(value = "SELECT user.id_user, user.nom_user, user.login,user.password,user.user_pic,user.role_id " +
            "FROM commande INNER JOIN plat_commande on plat_commande.commande_id = commande.id_commande INNER JOIN plat ON plat_commande.plat_id = plat.id_plat INNER JOIN user ON user.id_user=commande.consommateur_id " +
            "WHERE plat.restaurateur_id=:idRestaurateur GROUP BY user.id_user ORDER BY user.nom_user",nativeQuery = true)
    List<User> findAllCustomers(@Param("idRestaurateur") Long idrestaurateur);
}
