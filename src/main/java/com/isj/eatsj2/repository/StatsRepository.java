package com.isj.eatsj2.repository;

import com.isj.eatsj2.model.IConsommateurStats;
import com.isj.eatsj2.model.Plat;
import com.isj.eatsj2.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Repository
public interface StatsRepository extends JpaRepository<User,Long>{

    @Query(value = "SELECT r.depenses_totales AS depensesTotales,r.nombre_de_plats AS nombreDePlats,((r.nombre_de_plats*r.depenses_totales)/100) AS points,r.Consommateur,r.id_user AS idUser FROM(SELECT SUM(t.prix_totale) AS depenses_totales, SUM(t.qte_plats) AS nombre_de_plats, t.nom_user AS consommateur, t.id_user FROM " +
            "(SELECT plat_commande.plat_id, com.prix_totale,com.qte_plats, user.id_user, user.nom_user FROM `plat_commande`, (SELECT * FROM `commande` WHERE DATE(date_commande) >= DATE_SUB(DATE(NOW()), INTERVAL :days DAY)) com, user WHERE plat_commande.commande_id=com.id_commande and com.consommateur_id = user.id_user) " +
            "t GROUP BY t.id_user ORDER BY depenses_totales DESC) r",nativeQuery = true)
    List<IConsommateurStats> etudiantsFidelesSurNJours(@Param("days") int days);


    @Query(value = "SELECT r.depenses_totales AS depensesTotales,r.nombre_de_plats AS nombreDePlats,((r.nombre_de_plats*r.depenses_totales)/100) AS points,r.Consommateur,r.id_user AS idUser " +
            "                FROM( " +
            "                                SELECT SUM(t.prix_totale) AS depenses_totales, SUM(t.qte_plats) AS nombre_de_plats, t.nom_user AS consommateur, t.id_user " +
            "                FROM " +
            "                        (SELECT pl_com.plat_id, com.prix_totale,com.qte_plats, user.id_user, user.nom_user " +
            "                                FROM ( " +
            "                                 SELECT plat_commande.plat_id, plat_commande.commande_id " +
            "                                 FROM " +
            "                          ( " +
            "                                 SELECT plat.id_plat,plat.complemment,plat.nom_plat,plat.sauce,plat.plat_pic,plat.restaurateur_id " +
            "                                 FROM `plat` " +
            "                                 WHERE  plat.restaurateur_id= :idRestaurateur )p ,plat_commande WHERE plat_commande.plat_id = p.id_plat) pl_com, " +
            "                        (SELECT * FROM `commande` WHERE DATE(date_commande) >= DATE_SUB(DATE(NOW()), INTERVAL :days DAY)) com, user " +
            "                WHERE pl_com.commande_id=com.id_commande and com.consommateur_id = user.id_user) " +
            "                t GROUP BY t.id_user ORDER BY depenses_totales DESC) r",nativeQuery = true)
    List<IConsommateurStats> etudiantsFidelesSurNJoursPourUnVendeur(@Param("idRestaurateur") Long idRestaurateur, @Param("days") int days);



}
