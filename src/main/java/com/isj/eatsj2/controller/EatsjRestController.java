package com.isj.eatsj2.controller;

import java.io.IOException;
import java.util.*;

import com.isj.eatsj2.model.*;
import com.isj.eatsj2.services.*;

//import io.swagger.v3.oas.annotations.Operation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.multipart.MultipartFile;


//import javax.validation.constraints.NotNull;
import java.time.LocalDate;

import static com.isj.eatsj2.services.UsefulService.compressBytes;
import static com.isj.eatsj2.services.UsefulService.decompressBytes;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/eatsj/api")
@RestControllerAdvice

//http://localhost:8080/swagger-ui/index.html#/  Url Pour ouvrir Swagger
public class EatsjRestController {

    // @Autowired
    // private BoissonServiceImp boissonServiceImp;
    @Autowired
    private iBoissonService iBoissonService;
    @Autowired
    private iPlatService iPlatService;
    @Autowired
    private iCommandeService iCommandeService;
    @Autowired
    private iUserService iUserService;
    @Autowired
    private iRoleService iRoleService;
    @Autowired
    private iStatsService iStatsService;
    @Autowired
    private iCommentaireService iCommentaireService;
    @Autowired
    private iNoteService iNoteService;






    @GetMapping(value = "/boissons",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Boisson> listeBoissons() {
//        List<Boisson> listeBoissons = /*boissonServiceImp.listeBoissons();*/ iBoissonService.listeBoissons();
//        return listeBoissons;
        return iBoissonService.listeBoissons();
    }

    @GetMapping(value = "/commandes",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Commande> listeCommandes() {
        return iCommandeService.listeCommandes();
    }

    @GetMapping(value = "/plats",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> listePlats() {
//        List<Plat> listePlats = iPlatService.listePlats();
//        return listePlats;
        return iPlatService.listePlats();
    }

    @GetMapping(value = "/users",produces = MediaType.APPLICATION_JSON_VALUE)
    public  List<User> listeUsers(){
        List<User> userList =iUserService.listeUsers();
        return userList;
    }

    @GetMapping(value = "/roles",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Role> listeRole(){
        return iRoleService.listeRoles();
    }

    @GetMapping(value = "/commentaires/{idPlat}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Commentaire> commentairesDunPlat(@PathVariable Long idPlat){
        return iCommentaireService.commentaireDunPlat(idPlat);
    }

    @GetMapping(value = "/notes/{idPlat}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Note> notesDunPlat(@PathVariable Long idPlat){
        return iNoteService.notesDunPlat(idPlat);
    }

    @GetMapping(value = "/consommateurs",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> listeConsommateurs(){
        Role role = iRoleService.rechercherRole(2L);
        return role.getUsers();
    }

    @GetMapping(value = "/restaurateurs",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> listeRestaurateurs(){
        Role role = iRoleService.rechercherRole(1L);
        return role.getUsers();
    }

    @GetMapping(value = "/supprimerPlat/{id}")
    public List<Plat> deletePlat(@RequestParam  Long id){
        //long idPlat = Long.parseLong(id);
        return iPlatService.supprimerPlat(id);
    }

    @GetMapping(value = "/supprimerBoisson/{id}")
    public void deleteBoisson(@RequestParam (value = "id") String id){
        long idBoisson = Long.parseLong(id);
        iBoissonService.supprimerBoisson(idBoisson);
    }

    @GetMapping(value = "/supprimerUtilisateur/{id}")
    public List<User> deleteUser(@RequestParam (value = "id") Long idUser){
        return iUserService.supprimerUser(idUser);
    }

    @GetMapping(value = "/supprimerCommentaire/{id}")
    public void deleteCommentaire(@RequestParam (value = "id") String id){
        long idCommentaire = Long.parseLong(id);
        iCommentaireService.supprimerCommentaire(idCommentaire);
    }

    @GetMapping(value = "/supprimerNote/{id}")
    public void deleteNote(@RequestParam (value = "id") Long id){
        iNoteService.supprimerNote(id);
    }

    @GetMapping(value = "/platfavoris/{idRestaurateur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> platsFavoris(@PathVariable Long idRestaurateur){
//        List<Plat> platList = iPlatService.listePlatsFav(idRestaurateur);
//        return platList;
        return iPlatService.listePlatsFav(idRestaurateur);
    }

    @GetMapping(value = "/obtenirRoleduUser/{idUser}",produces = MediaType.APPLICATION_JSON_VALUE)
    public  Role obtenirRole(@PathVariable Long idUser){
        User user=iUserService.rechercherUser(idUser);
        return user.getRole();
    }

    @GetMapping(value = "/trouverUtilisateur/{login}/{mdp}",produces = MediaType.APPLICATION_JSON_VALUE)
    public UserAndRoleContainer trouverUtilisateur(@PathVariable String login,@PathVariable String mdp){
//        Map<String,UserAndRoleContainer> map = new HashMap<>();
        User user = new User() ;
        Role role =new Role();
        try {
            user = iUserService.trouverUserParIdentifiant(login,mdp);
            role =user.getRole();
        }catch (Exception e){
            System.out.println(e);
            role=null;
        }

        UserAndRoleContainer userAndRoleContainer = new UserAndRoleContainer(user,role);
        return userAndRoleContainer;
//        return  new UserAndRoleContainer();
    }


    @GetMapping(value = "/mesCommandes/{idUser}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Commande> mesCommandesConsommateur(@PathVariable Long idUser){
        User user = iUserService.rechercherUser(idUser);
        List<Commande> commandes = user.getCommandes();
//        LocalDate testdate;
//        for (Commande elt:commandes) {
//            testdate = UsefulService.convertDateToLocalDate(elt.getDateCommande());
//            System.out.println(testdate);
//        }
        commandes.sort(Comparator.comparing(Commande::getDateCommande).reversed());
        return commandes;
    }

   // @Operation(summary = "EN:Returns the leaderboards of consumers for all sellers over a certain period of time; \n" +
         //   "FR: Renvoi les leaderboards des consommateurs pour tous les restaurateurs réunis sur une periode de temps donné")

    @GetMapping(value = "commandesRestaurateur/{idRestaurateur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Commande> mesCommandesRestaurateur(@PathVariable Long idRestaurateur){
        List<Commande> commandeList = iCommandeService.toutesMesCommandes(idRestaurateur);
        return commandeList;
    }


    @GetMapping(value = "/mesClients/{idRestaurateur}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> mesClients(@PathVariable Long idRestaurateur){
        List<User> userList = iUserService.mesclients(idRestaurateur);

        for (User user:userList) {
            user.setCommandes(null);
            user.setPassword(null);
            user.setLogin(null);
        }

        return userList;
    }

    @GetMapping(value = "/rechercherPlat/{mot_cle}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> rechercherPlatParNom(@PathVariable String mot_cle){
        List<Plat> platList = iPlatService.rechercherPlatParNomPlat(mot_cle);
        return platList;
    }

   // @Operation(summary = "EN:Returns the leaderboards of consumers for all sellers over a certain period of time; \n" +
        //    "FR: Renvoi les leaderboards des consommateurs pour tous les restaurateurs réunis sur une periode de temps donné")

    @GetMapping(value = "/leaderBoardsConsommateursAbsolu/{nombreDeJours}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<IConsommateurStats> leaderBoardsAbsolu(@PathVariable int nombreDeJours){
        return iStatsService.leaderBoards(nombreDeJours);
    }

   // @Operation(summary = "EN:Returns the leaderboards of consumers for a particular seller over a certain period of time; \n" +
         //   "FR: Renvoi les leaderboards des consommateurs pour un restaurateur particuliér sur une periode de temps donné")
    @GetMapping(value = "/leaderBoardsConsommateurs/{idRestaurateur}/{nombreDeJours}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<IConsommateurStats> leaderBoards(@PathVariable Long idRestaurateur,@PathVariable int nombreDeJours){
        return iStatsService.leaderBoardsUnRestaurateur(idRestaurateur, nombreDeJours);
    }

    @GetMapping(value = "/mesPlats/{idUser}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> mesPlats(@PathVariable Long idUser){
        return iPlatService.mesPlats(idUser);
    }

    @GetMapping(value = "/mesBoissons/{idUser}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Boisson> mesBoissons(@PathVariable Long idUser){
        return iBoissonService.mesBoissons(idUser);
    }


    @GetMapping(value = "/platsfavorisplusieursjours/{idRestaurateur}/{nombreDeJours}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> favorisNJours(@PathVariable Long idRestaurateur,@PathVariable int nombreDeJours){
//        List<Plat> platList = iPlatService.platsFavorisSurNJours(Integer.parseInt(nombreDeJours));
//        List<Plat> platList = iPlatService.platsFavorisSurNJours(idRestaurateur,nombreDeJours);
        return iPlatService.platsFavorisSurNJours(idRestaurateur,nombreDeJours);
    }

    @GetMapping(value = "/platspargainargent/{idRestaurateur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> PlatsParOrdreArgent(@PathVariable Long idRestaurateur){
        return iPlatService.platsParOrdreVentes(idRestaurateur);
    }

    @GetMapping(value = "/platspargainargentsurplusieursjours/{idRestaurateur}/{nombreDeJours}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Plat> PlatsParOrdreArgentSurNJours(@PathVariable Long idRestaurateur,@PathVariable int nombreDeJours){
        return iPlatService.platsParOrdreVentesSurNJours(idRestaurateur, nombreDeJours);
    }

    @GetMapping(value = "/gaintotal/{idRestaurateur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Long gaintotal(@PathVariable Long idRestaurateur){
        Long sommeTotaleGagne = iPlatService.SommeTotaleGagne(idRestaurateur);
        if (sommeTotaleGagne == null){
            sommeTotaleGagne = 0L;
        }
        return sommeTotaleGagne;
    }

    @GetMapping(value = "/gaintotalsurplusieursjours/{idRestaurateur}/{nombreDeJours}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Long gaintotalSurNJours(@PathVariable Long idRestaurateur,@PathVariable int nombreDeJours){
        Long sommeTotaleGagne = iPlatService.SommeTotaleGagneSurNJours(idRestaurateur,nombreDeJours);
        if (sommeTotaleGagne == null){
            sommeTotaleGagne = 0L;
        }
        return sommeTotaleGagne;
    }

    @GetMapping(value = "/commandesAFaire/{idRestaurateur}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Commande> commandesAFaire(@PathVariable Long idRestaurateur){
        List<Commande> commandeList = iCommandeService.commandesNonLivre( idRestaurateur ,"PAS BON");
        return commandeList;
    }


    @PostMapping(value = "/enregistrerboisson")
    public Boisson addBoisson(@RequestBody Boisson boisson ){
        return iBoissonService.enregistrerBoisson(boisson);
    }

    @PostMapping(value = "/enregistrercommande/{idConsommateur}")

    public Commande addCommande(@RequestBody Commande commande, @PathVariable Long idConsommateur){
        System.out.println(commande);

        commande.setConsommateur(iUserService.rechercherUser(idConsommateur));
        return iCommandeService.enregistrerCommande(commande);
    }


    @PostMapping(value="/enregistrerNote")
    public Note addNote(@RequestBody Note note){
        return iNoteService.enregistrerNote(note);
    }


    @PostMapping(value="/enregistrerCommentaire")
    public Commentaire addCommentaire(@RequestBody Commentaire commentaire) {
        return iCommentaireService.enregistrerCommentaire(commentaire);
    }

    @PostMapping(value = "/enregistrerplat")
    public Plat addPlat(/*@RequestBody Plat plat,*/@RequestPart("plat")Plat plat, @RequestPart("imageFile")MultipartFile file) throws IOException {
        plat.setPlatPic(file.getBytes());
        return iPlatService.enregistrerPlat(plat);
    }

    @PostMapping(value = "/enregistrerutilisateur/{idRole}")
    public User addUser(/*@RequestBody User user,*/@RequestPart("user")User user,@RequestPart("imageFile")MultipartFile file, @PathVariable Long idRole) throws IOException {
        user.setRole(iRoleService.rechercherRole(idRole));
        user.setUserPic(file.getBytes());
//        user.setUserPic(compressBytes(file.getBytes()));
        return iUserService.enregistrerUser(user);
    }

    @PostMapping(value = "/ppUtilisateur/{idUser}")
    public User changerPicUser(@RequestPart("imageFile")MultipartFile file, @PathVariable Long idUser) throws IOException {
        User user = iUserService.rechercherUser(idUser);
        user.setUserPic(file.getBytes());
//        user.setUserPic(compressBytes(file.getBytes()));
        return iUserService.enregistrerUser(user);
    }

    @PostMapping(value = "/modifierEtatCommandes")
    public List<Commande> changerEtatCommandes(@RequestBody List<Commande> commandes){
        for (Commande commande:commandes) {
            commande.setEtat("OKAY");
            iCommandeService.enregistrerCommande(commande);
        }

        return iCommandeService.listeCommandes();
    }


    //@Operation(summary = "Ceci est une fonction pour tester ceraines choses, c'est tout.")
    @GetMapping("/test")

//    @Operation(summary = "Ceci est une fonction pour tester ceraines choses, c'est tout.")
//    @GetMapping("/test")

    public RedirectView localRedirect(){
//        User user= new User("Cedric Armel","dozo","0000");
//        user.setRole(iRoleService.rechercherRole(1L));
//        iUserService.enregistrerUser(user);
//        System.out.println(user);
//        List<Commande> commandeList = iCommandeService.listeCommandes();
//        Commande commande = iCommandeService.rechercherCommande(14L);
//        Plat plat=iPlatService.rechercherPlat(4L);
//        if (commande == null || plat == null){
//            System.out.println("Null");
//        }
//        else {
//            List<Plat> plats = commande.getPlatsCommande();
//            plats.add(plat);
//            commande.setPlatsCommande(plats);
//            iCommandeService.enregistrerCommande(commande);
//            System.out.println(commande.getPlatsCommande().size());
////        }
//        iRoleService.enregistrerRole(new Role("Restaurateur"));
//        iRoleService.enregistrerRole(new Role("Consommateur"));
//        iRoleService.enregistrerRole(new Role("Administrateur"));
        RedirectView redirectView = new RedirectView();
        redirectView.setUrl("http://localhost:8080/eatsj/api/roles");
        return redirectView;
    }

}
